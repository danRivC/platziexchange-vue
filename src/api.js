const url = "https://api.coincap.io/v2";

const getAssets = async () => {
  const response = await fetch(`${url}/assets?limit=20`);
  const result = await response.json();

  return result;
};

const getAsset = async (coin) => {
  const response = await fetch(`${url}/assets/${coin}`);
  const result = await response.json();

  return result;
};

const getAssetHistory = async (coin) => {
  const now = new Date();
  const end = now.getTime();
  now.setDate(now.getDate() - 1);
  const start = now.getTime();
  const response = await fetch(
    `${url}/assets/${coin}/history?interval=h1&start=${start}&end=${end}`
  );
  const result = await response.json();
  return result;
};

const getMarkets = async (coin) => {
  const response = await fetch(`${url}/assets/${coin}/markets?limit=5`);
  const result = await response.json();
  return result;
};

const getExchange = async (id) => {
  const res = await fetch(`${url}/exchanges/${id}`);
  const result = await res.json();

  return result;
};

export default {
  getAssets,
  getAsset,
  getAssetHistory,
  getMarkets,
  getExchange,
};
